from pickle import dump, load

class NumberRepository:
    """
    Aitab meeles hoida arve ja leida nende põhjal erinevaid suuruseid.
    Võimaldab arve salvestada faili ning vajadusel neid sealt taastada.
    """


    def __init__(self):
        """
        Uue arvurepositooriumi loomine
        :return: uus repositoorium
        """
        self.numbers = []

    def add(self, newNumber):
        """
        Uue arvu lisamine repositooriumisse
        :param newNumber: lisatav arv
        :return: None
        """
        self.numbers.append(newNumber)

    def min(self):
        """
        Leiab miinimumi repositooriumi arvu
        :return: arvude miinimumi
        """
        return min(self.numbers)

    def max(self):
        return max(self.numbers)

    def avg(self):
        return sum(self.numbers) / len(self.numbers)

    def saveToFile(self, filename):
        f = open(filename, 'w')
        dump(self.numbers, f)
        f.close()

    def loadFromFile(self, filename):
        f = open(filename)
        self.numbers = load(filename)
        f.close()
